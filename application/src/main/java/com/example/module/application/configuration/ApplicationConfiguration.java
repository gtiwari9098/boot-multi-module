package com.example.module.application.configuration;


import com.example.module.application.MyBean;
import com.example.module.application.MyBeanConsumer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*

@Configuration: Tags the class as a source of bean definitions for the application context.

@EnableAutoConfiguration: Tells Spring Boot to start adding beans based on classpath settings, other beans, and various property settings. For example, if spring-webmvc is on the classpath, this annotation flags the application as a web application and activates key behaviors, such as setting up a DispatcherServlet.

 */
@Configuration
public class ApplicationConfiguration {

    @Bean
    public MyBean myBean() {
        return new MyBean();
    }

    @Bean
    public MyBeanConsumer myBeanConsumer() {
        return new MyBeanConsumer(myBean());
    }

}