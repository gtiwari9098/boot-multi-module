package com.example.module.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// @ComponentScan: Tells Spring to look for other components, configurations, and services in the com/example package, letting it find the controllers.
//@SpringBootApplication is a convenience annotation that adds all of the following:


@SpringBootApplication(scanBasePackages = {"com.example.module","com.example.multimodule.service"})
public class ApplicationMain {
    public static void main(String[] args) {
        SpringApplication.run(com.example.module.main.ApplicationMain.class, args);
    }
}
